<?php

// ==============================================
// Load database information and local dev params
// ==============================================

/**
 * Get the database details based on the domain the site
 * resides on. E.g. Staging details for the staging domain etc.
 */
include(dirname(__FILE__).'/'.$_SERVER['SERVER_NAME'].'-config.php');

// ========================
// Custom content directory
// ========================
define('WP_CONTENT_DIR', dirname( __FILE__ ).'/content');
define('WP_CONTENT_URL', 'http://'.$_SERVER['HTTP_HOST'].'/content');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY', 'put your unique phrase here');
define('SECURE_AUTH_KEY', 'put your unique phrase here');
define('LOGGED_IN_KEY', 'put your unique phrase here');
define('NONCE_KEY', 'put your unique phrase here');
define('AUTH_SALT', 'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT', 'put your unique phrase here');
define('NONCE_SALT', 'put your unique phrase here');

// ============
// Table prefix
// ============
$table_prefix = 'wp_';

// ========
// Language
// ========
define('WPLANG', '');

// =========
// Debugging
// =========
if(defined('WP_LOCAL_DEV') && WP_LOCAL_DEV)
	define('WP_DEBUG', true);
else
	define('WP_DEBUG', false);

// ================================
// Prevent filed editing form admin
// ================================
define('DISALLOW_FILE_EDIT', true);

// ====
// URLS
// ====
if(!defined('WP_SITEURL'))
	define('WP_SITEURL', 'http://'.$_SERVER['HTTP_HOST'].'/wordpress');

if(!defined('WP_HOME'))
	define('WP_HOME', 'http://'.$_SERVER['HTTP_HOST']);

// ==================
// Path to MU plugins
// ==================
define('WPMU_PLUGIN_DIR', dirname(__FILE__).'/content/plugins-mu');
define('WPMU_PLUGIN_URL', WP_HOME.'/content/plugins-mu');

// ===================
// Bootstrap WordPress
// ===================
if(!defined('ABSPATH'))
	define('ABSPATH', dirname(__FILE__).'/wordpress/');
require_once(ABSPATH.'wp-settings.php');