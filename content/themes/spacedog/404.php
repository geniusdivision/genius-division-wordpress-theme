<article class="error-404">
    <h1><?php _e('Page Not Found: Error 404', 'spacedog'); ?></h1>
    <p><?php _e('The page you are trying to reach may have been moved, deleted or does not exist.', 'spacedog'); ?></p>
    <p><?php _e('Try heading back to the <a href="'.get_bloginfo('url').'">homepage</a> to see if you can find what you are looking for from there.', 'spacedog'); ?></p>
    <?php get_search_form(); ?>
</article>
<!-- /.error-404 -->