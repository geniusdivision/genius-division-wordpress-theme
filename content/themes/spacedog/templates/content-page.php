<?php while(have_posts()) : the_post(); ?>
<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
    <header>
        <h1 class="entry-title" itemprop="name headline"><?php the_title(); ?></h1>
    </header>
    <div class="entry-content" itemprop="articleBody">
        <?php the_content(); ?>
    </div>
    <footer class="entry-footer">
        <?php wp_link_pages(array(
            'before'            => '<ol class="nav nav--pagination">',
            'after'             => '</ol>',
            'link_before'       => '<li>',
            'link_after'        => '</li>',
            'next_or_number'    => 'number',
            'nextpagelink'     => __('Next page', 'spacedog'),
            'previouspagelink' => __('Previous page', 'spacedog'),
            'pagelink'         => '%'
        )); ?>
    </footer>
</article>
<!-- /article -->
<?php endwhile; ?>