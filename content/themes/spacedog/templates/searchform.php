<form method="get" class="search" action="<?php echo esc_url(home_url('/')); ?>" role="search">
	<label for="s" class="screen-reader-text"><?php _ex('Search', 'assistive text', 'spacedog'); ?></label>
	<input type="search" class="search-input" name="s" value="<?php echo esc_attr(get_search_query()); ?>" id="s" placeholder="<?php echo esc_attr_x('Search&hellip;', 'spacedog'); ?>" />
	<input type="submit" class="btn btn--search" value="<?php echo esc_attr_x('Search', 'spacedog'); ?>" />
</form>
<!-- /.search-form -->