<?php while(have_posts()) : the_post(); ?>
<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
    <header>
        <h2 class="entry-title" itemprop="name headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-summary" itemprop="articleSummary">
        <?php the_excerpt(); ?>
    </div>
    <?php if(the_tags()) : ?>
    <footer>
        <?php the_tags('<ul class="entry-tags"><li>','</li><li>','</li></ul>'); ?>
    </footer>
    <?php endif; ?>
</article>
<!-- /article -->
<?php endwhile; ?>

<?php if(!have_posts()) : ?>
<article class="error-404">
    <h2><?php _e('Sorry, no results were found.', 'spacedog'); ?></h2>
    <?php get_search_form(); ?>
</article>
<!-- /.error-404 -->
<?php endif; ?>

<?php if($wp_query->max_num_pages > 1) : ?>
<nav class="post-nav">
    <ul class="nav nav--pagination">
    <?php if (get_next_posts_link()) : ?>
        <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'spacedog')); ?></li>
    <?php endif; ?>
    <?php if (get_previous_posts_link()) : ?>
        <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'spacedog')); ?></li>
    <?php endif; ?>
    </ul>
</nav>
<!-- /.post-nav -->
<?php endif; ?>