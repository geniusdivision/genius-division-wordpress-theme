<footer class="main-footer" role="contentinfo">
    <p>&copy; <?php echo date('Y').' '.get_bloginfo('name'); ?></p>
</footer>
<!-- /.main-footer -->
<?php wp_footer(); ?>

</body>
</html>