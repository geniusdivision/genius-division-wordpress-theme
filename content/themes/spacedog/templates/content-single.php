<?php while(have_posts()) : the_post(); ?>
<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
    <header>
        <h1 class="entry-title" itemprop="name headline"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content" itemprop="articleBody">
        <?php the_post_thumbnail(); ?>
        <?php the_content(); ?>
    </div>
    <footer class="entry-footer">
        <?php the_tags('<ul class="entry-tags"><li>','</li><li>','</li></ul>'); ?>
        <?php wp_link_pages(array(
            'before'            => '<ol class="nav nav--pagination">',
            'after'             => '</ol>',
            'link_before'       => '<li>',
            'link_after'        => '</li>',
            'next_or_number'    => 'number',
            'nextpagelink'     => __('Next page', 'spacedog'),
            'previouspagelink' => __('Previous page', 'spacedog'),
            'pagelink'         => '%'
        )); ?>
    </footer>
</article>
<!-- /article -->
<?php comments_template('/templates/comments.php'); ?>
<?php endwhile; ?>