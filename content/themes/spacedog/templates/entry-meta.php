<p class="entry-meta">
	Posted on
	<time datetime="<?php echo the_time('j-m-Y'); ?>" itemprop="datePublished">
		<?php the_time('jS F, Y'); ?>
	</time>
	by
	<span itemprop="author" itemtype="http://schema.org/Person">
		<span itemprop="name">
			<?php the_author(); ?>
		</span>
	</span>
</p>