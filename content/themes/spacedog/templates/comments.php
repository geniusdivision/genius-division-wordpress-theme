<?php if(!post_password_required()) : ?>
<aside class="comments">
	<?php if(have_comments()) : ?>
	<h3><?php _e('Comments', 'spacedog'); ?></h3>
	<?php if(get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
	<ul class="nav nav--pagination">
		<li><?php previous_comments_link(__('&larr; Older Comments', 'spacedog')); ?></li>
		<li><?php next_comments_link(__('Newer Comments &rarr;', 'spacedog')); ?></li>
	</ul>
	<?php endif; ?>
	<ol class="comments-list">
		<?php wp_list_comments(); ?>
	</ol>
	<?php if(get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
	<ul class="nav pagination">
		<li><?php previous_comments_link(__('&larr; Older Comments', 'spacedog')); ?></li>
		<li><?php next_comments_link(__('Newer Comments &rarr;', 'spacedog')); ?></li>
	</ul>
	<?php endif; ?>
	<?php else : ?>
	<?php if(comments_open()) : ?>
	<p>No one as posted a comment yet. Be the first?</p>
	<?php else : ?>
	<p>Comments are closed on this article.</p>
	<?php endif; ?>
	<?php endif; ?>
	<?php comment_form(); ?>
</aside>
<!-- /.comments -->
<?php endif; ?>