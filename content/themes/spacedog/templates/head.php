<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="no-js ie6 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->

<head>

	<!-- =================================================

	<?php echo bloginfo('name'); ?>

	Author: Genius Division
	Version: 1.0
	URL: <?php echo home_url(); ?>
	
	License: http://creativecommons.org/licenses/MIT/

	================================================== -->

	<meta charset="<?php echo bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- Prefetch DNS For External Assets
	====================================-->
	<link rel="dns-prefetch" href="//www.google-analytics.com" />
	<link rel="dns-prefetch" href="//code.google.com" />
	<link rel="dns-prefetch" href="//cdnjs.cloudflare.com" />

	<!-- Basic Page Needs
	====================================-->
	<title><?php wp_title(' | '); ?></title>
	<meta name="author" content="Genius Division" />

	<!-- Mobile Specific
	====================================-->
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- CSS
	====================================-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/screen.css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="http://universal-ie6-css.googlecode.com/files/ie6.1.1.css" media="screen, projection"><![endif]-->

	<!-- Javascript
	====================================-->
	<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
	<!--[if lt IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script><![endif]-->

	<!-- Favicons
	====================================-->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/apple-touch-icon-144x144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/apple-touch-icon-114x114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/apple-touch-icon-72x72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/apple-touch-icon-57x57-precomposed.png" />
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/favicon.png" />

	<!-- Google Analytics
	====================================-->
	<script>
		var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src='//www.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>

	<!-- Injected by WordPress
	====================================-->
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <div class="container">

    	<header class="main-header" role="banner">
		    <nav class="main-navigation" role="navigation">
				<?php wp_nav_menu(array(
					'menu'			=> 'primary-navigation',
					'container'		=> '',
					'menu_class'	=> 'nav'
				)); ?>
		    </nav>
		    <!-- ./primary-navigation -->
		</header>
		<!-- end /.main-header -->