<?php

/**
 * Utilities functions
 * 
 * @package     WordPress
 * @subpackage  Genius Division Boilerplate
 * @author      Genius Division <us@geniusdivision.com>
 */

/**
 * Template Routing
 * Based on that of the Roots WordPress Framework
 */

/**
 * Grab the main template path
 */
function gd_template_path() {
	return gd_wrapping::$main_template;
}

/**
 * Now for the magic.
 * This lets us use base.php for for all templates
 * helps achieve a more modular workflow.
 */

class gd_wrapping {

  // store the full path to the main template file
  static $main_template;

  // store the base name of the template file; e.g. 'page' for 'page.php' etc.
  static $base;

  static function wrap($template) {

    self::$main_template = $template;
    self::$base = substr(basename(self::$main_template), 0, -4);

    if (self::$base === 'index') {
      self::$base = false;
    }

    $templates = array('base.php');

    if (self::$base) {
      array_unshift($templates, sprintf('base-%s.php', self::$base));
    }

    return locate_template($templates);

  }

}

add_filter('template_include', array('gd_wrapping', 'wrap'), 99);