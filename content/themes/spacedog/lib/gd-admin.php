<?php

/**
 * Genius Division Admin functions
 * 
 * @package     WordPress
 * @subpackage  Genius Division Boilerplate
 * @author  	Genius Division <us@geniusdivision.com>
 */

/** 
 * Tell WordPress to use different login styles
 */
function gd_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_url').'/assets/css/admin.min.css">';
}
add_action('login_head', 'gd_login_css');

add_filter('login_headerurl', 'gd_login_logo_url');
function gd_login_logo_url($url) {
	return 'http://geniusdivision.com/';
}

/**
 * Remove login errors for increased security
 */
add_filter('login_errors','login_error_message');

function login_error_message($error){
    // check if that's the error you are looking for
    $pos = strpos($error, 'incorrect');
    if (is_int($pos)) {
        //its the right error so you can overwrite it
        $error = "Sorry! That username &amp; password combination doesn't exist. <a title='Password Lost and Found' href='http://wp.dev/wordpress/wp-login.php?action=lostpassword'>Lost your password</a>?";
    }
    return $error;
}

/**
 * Help Widget
 */
function gd_help_widget() {
	echo '<p>This site was built and developed by Genius Division.</p>';
	echo '<p><strong>Call:</strong> 01226 244009<br/><strong>Email:</strong> <a href="mailto:us@geniusdivision.com">us@geniusdivision.com</a></p>';
}

function gd_add_help_widget() {
	add_meta_box('gd_help_dashboard_widget', 'Help & Support', 'gd_help_widget', 'dashboard', 'side', 'high');
}
add_action('wp_dashboard_setup', 'gd_add_help_widget');