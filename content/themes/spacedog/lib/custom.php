<?php

/**
 * Custom functions
 * 
 * @package     WordPress
 * @subpackage  Genius Division Boilerplate
 * @author      Genius Division <us@geniusdivision.com>
 */
 
 
 function gd_setup() {

  // Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
  register_nav_menus(array(
    'primary_navigation' => __('Primary Navigation', 'spacedog')
  ));

  // Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
  add_theme_support('post-thumbnails');
  // set_post_thumbnail_size(150, 150, false);
  // add_image_size('category-thumb', 300, 9999); // 300px wide (and unlimited height)

  // Add post formats (http://codex.wordpress.org/Post_Formats)
  // add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

  // Add feed links
  add_theme_support('automatic-feed-links');

  // Register the default sidebar
  register_sidebar(array(
    'name'          => __('Default Sidebar', 'spacedog'),
    'description'   => 'The default sidebar displayed on the website.',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ));

}
add_action('after_setup_theme', 'gd_setup');