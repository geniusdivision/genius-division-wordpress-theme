<?php

/**
 * Scripts functions
 * 
 * @package     WordPress
 * @subpackage  Genius Division Boilerplate
 * @author      Genius Division <us@geniusdivision.com>
 */

/**
 * Enqueue scripts and stylesheets
 *
 * Enqueue scripts in the following order:
 * 1. jquery-1.9.1.min.js via Google CDN
 * 4. /theme/assets/scripts/scripts.js (in footer)
 */
function gd_scripts() {
  if(!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js', false, null, true);
    add_filter('script_loader_src', 'gd_jquery_fallback', 10, 2);
  }

  if(is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_register_script('gd_plugins', get_template_directory_uri() . '/assets/scripts/plugins.js', false, null, true);
  wp_register_script('gd_main', get_template_directory_uri() . '/assets/scripts/scripts.js', false, null, true);
  wp_enqueue_script('jquery');
  wp_enqueue_script('gd_plugins');
  wp_enqueue_script('gd_main');
}
add_action('wp_enqueue_scripts', 'gd_scripts', 100);

// http://wordpress.stackexchange.com/a/12450
function gd_jquery_fallback($src, $handle) {
  static $add_jquery_fallback = false;

  if($add_jquery_fallback) {
    echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/scripts/vendor/jquery-1.8.3.min.js"><\/script>\')</script>' . "\n";
    $add_jquery_fallback = false;
  }

  if($handle === 'jquery') {
    $add_jquery_fallback = true;
  }

  return $src;
}