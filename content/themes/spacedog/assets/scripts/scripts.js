/**
* Scripts
*/

/**
 * JavaScript Dispacher - triggers scripts based on the class of the <body>
 * @author Richard Keys - Genius Division - http://www.geniusdivision.com
 */
bd = {
	common : {
		init : function() {

			// Responsive Menu
			$('.main-navigation ul').mobileMenu({
				switchWidth: 640,
				prependTo: '.main-navigation',
				method: 'menu',
				topOptionText: 'Navigate...'
			});

			// Hide the address bar on mobile browsers
    		setTimeout(function(){
    			window.scrollTo(0, 1);
    		}, 0);

		}
	},
	single : {
		init : function() {

		}
	}
};

UTIL = {
	fire : function(func,funcname, args){
		//change to match above namespace
		var namespace = bd;
		funcname = (funcname === undefined) ? 'init' : funcname;
		if (func !== '' && namespace[func] && typeof namespace[func][funcname] == 'function'){
			namespace[func][funcname](args);
		} 

	}, 
	loadEvents : function(){
		// hit up common first.
		UTIL.fire('common');
		// do all the classes too.
		$.each(document.body.className.split(/\s+/),function(i,classnm){
			UTIL.fire(classnm);
		});
	}
};
$(document).ready(UTIL.loadEvents);