<?php

/**
 * Include functions
 * 
 * @package 	WordPress
 * @subpackage 	Genius Division Boilerplate
 * @author  	Genius Division <us@geniusdivision.com>
 */

require_once locate_template('/lib/utilities.php');       // Utility functions
require_once locate_template('/lib/cleanup.php');         // Cleanup functions
require_once locate_template('/lib/gd-admin.php');        // Genius Division admin functions
require_once locate_template('/lib/scripts.php');         // Scripts functions
require_once locate_template('/lib/custom.php');          // Functions specific to this theme