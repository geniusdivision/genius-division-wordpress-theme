<?php

define('DB_NAME', 'dev_wp');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');

define('WP_SITEURL', 'http://www.wp-boilerplate.loc/wordpress');
define('WP_HOME', 'http://www.wp-boilerplate.loc');

define('WP_LOCAL_DEV', true);